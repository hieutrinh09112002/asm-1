﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class NguoiLaoDong
{
    public string HoTen { get; set; }
    public int NamSinh { get; set; }
    public double LuongCoBan { get; set; }

    public NguoiLaoDong()
    {
    }

    public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
    {
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }

    public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
    {
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }

    public double TinhLuong()
    {
        return LuongCoBan;
    }

    public void XuatThongTin()
    {
        Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
    }
}

class GiaoVien : NguoiLaoDong
{
    public double HeSoLuong { get; set; }

    public GiaoVien()
    {
    }

    public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        : base(hoTen, namSinh, luongCoBan)
    {
        HeSoLuong = heSoLuong;
    }

    public new void NhapThongTin(double heSoLuong)
    {
        HeSoLuong = heSoLuong;
    }

    public new double TinhLuong()
    {
        return LuongCoBan * HeSoLuong * 1.25;
    }

    public new void XuatThongTin()
    {
        base.XuatThongTin();
        Console.WriteLine($"He so luong: {HeSoLuong}, Luong: {TinhLuong()}");
    }

    public double XuLy()
    {
        return HeSoLuong + 0.6;
    }
}

class Program
{
    static void Main(string[] args)
    {
        Console.Write("Nhap so luong giao vien: ");
        int soLuong = int.Parse(Console.ReadLine());

        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 0; i < soLuong; i++)
        {
            Console.WriteLine("******************************");
            Console.WriteLine($"Nhap thong tin cho giao vien {i + 1}");
            Console.Write("Ho ten: ");
            string hoTen = Console.ReadLine();
            Console.Write("Nam sinh: ");
            int namSinh = int.Parse(Console.ReadLine());
            Console.Write("Luong co ban: ");
            double luongCoBan = double.Parse(Console.ReadLine());
            Console.Write("He so luong: ");
            double heSoLuong = double.Parse(Console.ReadLine());
            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);


        }


        GiaoVien giaoVienCoLuongThapNhat = danhSachGiaoVien[0];
        foreach (var gv in danhSachGiaoVien)
        {
            if (gv.TinhLuong() < giaoVienCoLuongThapNhat.TinhLuong())
            {
                giaoVienCoLuongThapNhat = gv;
            }
        }

        Console.WriteLine("Thong tin giao vien co luong thap nhat:");
        giaoVienCoLuongThapNhat.XuatThongTin();

        Console.WriteLine("Nhan mot phim bat ky de ket thuc chuong trinh...");
        Console.Read();
    }
}
//test 1
//test 2
//test 3